﻿namespace Phonebook.Models
{
    public class Entity
    {
        public int Id { get; set; }
        public byte[] RowVersion { get; set; }
    }
}