﻿1. Install EF
2. Add "Search Personnel" link.
3. Add Personnel, Department models. Entity superclass as usual.
4. Add "DAL" folder, PhonebookContext.cs
5. Add Initializer and Seed data.
6. Add initializer to web.config.
7. Add ConnectionString to web.config.
8. Fixed Initializer
9. Added Search Action (list everything)
10. Add ViewModels folder, Add SearchPersonnel ViewModel
11. Add Where clause in PersonnelsController.Search Action
12. Try that query string is bound to view model by adding "?FirstName=Aaron&LastName=Mitchell"
13. Add _SearchForm partial view with model SearchPersonnel
14. pass SearchForm in ViewData
15. render partial _SearchForm in Search
16. Install-Package CaptchaMvc.Mvc5 -Version 1.5.0
    http://vvson.net/Projects/en-US/Project/captchamvc
17. Captcha validation
18. File upload no ajax
19. file download
20. 
21. 
