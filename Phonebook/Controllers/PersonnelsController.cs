﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Phonebook.DAL;
using Phonebook.Models;
using Phonebook.ViewModels;

namespace Phonebook.Controllers
{
    public class PersonnelsController : Controller
    {
        private PhonebookContext db = new PhonebookContext();

        public ActionResult Photo(int id)
        {
            var personnel = db.Personnels.Find(id);

            return File(
                fileContents: personnel.Photo, 
                contentType: System.Net.Mime.MediaTypeNames.Image.Jpeg, 
                fileDownloadName: $"{id}.jpg");
        }


        public ActionResult UploadPhoto()
        {
            var personnels = db.Personnels.ToList();
            ViewData["Personnels"] = personnels;
            return View();
        }

        [HttpPost]
        public ActionResult UploadPhoto(int personnelId, HttpPostedFileBase file)
        {
            var personnel = db.Personnels.Find(personnelId);

            byte[] fileData;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            personnel.Photo = fileData;

            db.SaveChanges();

            return RedirectToAction(nameof(PersonnelsController.Index));
        }


        // GET: Personnels
        public ActionResult Index()
        {
            return View(db.Personnels.ToList());
        }

        // GET: Personnels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personnel personnel = db.Personnels.Find(id);
            if (personnel == null)
            {
                return HttpNotFound();
            }
            return View(personnel);
        }

        // GET: Personnels/Create
        public ActionResult Create()
        {
            return View();
        }

        [CaptchaMvc.Attributes.CaptchaVerify("Captcha is not valid")]
        public ActionResult Search(SearchPersonnel searchPersonnel)
        {
            if (!ModelState.IsValid)
            {
                TempData["ErrorMessage"] = "captcha is not valid.";
                return View();
            }

            var personnels = db.Personnels
                .Where(p => p.FirstName == searchPersonnel.FirstName)
                .Where(p => p.LastName == searchPersonnel.LastName)
                .Include(p => p.Department)
                .ToList();

            ViewData["SearchPersonnel"] = searchPersonnel;
            return View(personnels);
        }

        // POST: Personnels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Title,PhoneNumber,Email,RowVersion")] Personnel personnel)
        {
            if (ModelState.IsValid)
            {
                db.Personnels.Add(personnel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(personnel);
        }

        // GET: Personnels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personnel personnel = db.Personnels.Find(id);
            if (personnel == null)
            {
                return HttpNotFound();
            }
            return View(personnel);
        }

        // POST: Personnels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Title,PhoneNumber,Email,RowVersion")] Personnel personnel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(personnel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(personnel);
        }

        // GET: Personnels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personnel personnel = db.Personnels.Find(id);
            if (personnel == null)
            {
                return HttpNotFound();
            }
            return View(personnel);
        }

        // POST: Personnels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Personnel personnel = db.Personnels.Find(id);
            db.Personnels.Remove(personnel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
