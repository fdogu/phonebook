﻿using Phonebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Phonebook.DAL
{
    public class PhonebookContext : DbContext
    {
        public DbSet<Personnel> Personnels { get; set; }
        public DbSet<Department> Departments { get; set; }
    }
}